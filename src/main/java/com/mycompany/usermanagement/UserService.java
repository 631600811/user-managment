package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author exhau
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();
    static{
        userList.add(new User("admin", "password"));
    }


    public static boolean addUser(User user) {
        if(!user.getUsername().equals("admin"))
        userList.add(user);
        return true;
    }

    public static boolean addUser(String username, String password) {
        userList.add(new User(username, password));
        return true;
    }

    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    public static User getUser(int index) {
        if (index > userList.size() - 1)
        {
            return null;
        }
        return userList.get(index);
    }

    public static ArrayList<User> getUsers() {
        return userList;
    }

    public static ArrayList<User> getUsers(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList)
        {
            if (user.getUsername().startsWith(searchText))
            {
                list.add(user);
            }
        }
        return userList;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    public static User login(String userName, String password) {
        for (User user : userList)
        {
            if (user.getUsername().equals(userName) && user.getPassword().equals(password))
            {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;

        try
        {   
            File file = new File("User.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex)
        {

        } catch (IOException ex)
        {

        } finally
        {
            try
            {
                fos.close();
            } catch (IOException ex)
            {

            }
        }

    }

    public static void load() {
        FileInputStream fis = null;
        try
        {   
            File file = new File("User.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex)
        {
            
        } catch (IOException ex)
        {
            
        } catch (ClassNotFoundException ex)
        {
            
        } finally
        {
            try
            {
                if(fis != null){
                    fis.close();
                }
            } catch (IOException ex)
            {
                
            }
        }
    }

}
