/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;


/**
 *
 * @author exhau
 */
public class TestUserService {
    public static void main(String[] args) {
        FileInputStream fis = null;
        ArrayList<User> userList = null;
        try
        {   
            File file = new File("User.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            System.out.println(userList);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex)
        {
            
        } catch (IOException ex)
        {
            
        } catch (ClassNotFoundException ex)
        {
            
        } finally
        {
            try
            {
                if(fis != null){
                    fis.close();
                }
            } catch (IOException ex)
            {
                
            }
        }
    }
}
